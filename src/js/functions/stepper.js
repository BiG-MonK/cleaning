
export const stepper = (input) => {
    const minus = input.closest('.stepper').querySelector('.stepper__btn--minus')
    const plus = input.closest('.stepper').querySelector('.stepper__btn--plus')


    if (input.value === '1') minus.classList.add('stepper__btn--disabled')
    if (input.value === '99') plus.classList.add('stepper__btn--disabled')

    // Обработка ввода в поле input (stepper)
    input.addEventListener('keyup', e => {
        if (e.currentTarget.value <= 0) {
            minus.classList.add('stepper__btn--disabled')
            plus.classList.remove('stepper__btn--disabled')
            e.currentTarget.value = 0
        } else {
            minus.classList.remove('stepper__btn--disabled')
        }

        if (e.currentTarget.value > 99) {
            minus.classList.remove('stepper__btn--disabled')
            plus.classList.add('stepper__btn--disabled')
            e.currentTarget.value = 99
        } else {
            plus.classList.remove('stepper__btn--disabled')
        }
    })

    // Обработка ввода на потерю фокуса в поле input (stepper)
    input.addEventListener('blur', e => {
        if ((e.currentTarget.value <= 0) || (!Number.isInteger(+(e.currentTarget.value)))) {
            e.currentTarget.value = 0
        }
        if (e.currentTarget.value > 99) {
            e.currentTarget.value = 99
            plus.classList.add('stepper__btn--disabled')
        }
    })

    // Сохранение старого значения input в data атрибуте
    input.addEventListener('focus', e => {
        e.target.dataset.prevValue = e.target.value
    })

    // Увеличение значения поля input (stepper)
    plus.addEventListener('click', e => {
        // кастомный event для перехвата программного изменения input
        let event = new Event('input')
        input.dispatchEvent(event)

        let currentValue = parseInt(input.value)
        currentValue++
        input.value = currentValue
        minus.classList.remove('stepper__btn--disabled')
        if (input.value > 98) {
            input.value = 99
            plus.classList.add('stepper__btn--disabled')
        } else {
            plus.classList.remove('stepper__btn--disabled')
        }
    })

    // Уменьшение значения поля input (stepper)
    minus.addEventListener('click', e => {
        let currentValue = parseInt(input.value)
        currentValue--
        input.value = currentValue
        plus.classList.remove('stepper__btn--disabled')
        if (input.value <= 0) {
            input.value = 0
            minus.classList.add('stepper__btn--disabled')
        } else {
            minus.classList.remove('stepper__btn--disabled')
        }
    })
}