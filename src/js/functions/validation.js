
const validateName = (element) => {
    let name = element.value
    const re = /^[a-zёЁа-я\- ]+$/
    return re.test(String(name).toLowerCase())
}

// const validatePhone = (element) => {
//     let phone = element.value
//     const re = /^((\+?7|8)[ \-]?)?([ \-])?(\d{6}|(\d{3}([ \-])?\d{3})?[\- ]?\d{2}[\- ]?\d{2})$/
//     return re.test(String(phone).toLowerCase())
// }

// --- валидация поля
export const isValidate = (element) => {
    if (element.value.length > 2) {
        if (element.name === 'name') return validateName(element)
        // if (element.name === 'phone') return validatePhone(element)
        return true
    } else return false
}