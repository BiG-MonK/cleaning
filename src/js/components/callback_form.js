import Inputmask from "../vendor/inputmask.min"
import {isValidate} from "../functions/validation"

const
    serviceForm = document.querySelector('#serviceForm'),
    dynamicCallForm = document.querySelector('#dynamicCallForm'),
    class_AnimateBtnActive = "service-btn--active",
    phoneFields = document.querySelectorAll('.callback-form__field[name="phone"]'),
    nameFields = document.querySelectorAll('.callback-form__field[name="name"]'),
    policyCheckBoxes = document.querySelectorAll('.callback-form__policy-checkbox[name="policy"]'),
    errorColor = "rgb(214, 75, 75)",
    successColorDynamicForm = "rgb(0, 219, 254)",
    successColorServiceForm = "rgb(109, 139, 138)"

// Подключение и валидация полей телефонов
let im = new Inputmask("+7 (999) 999 99-99", {
    "onincomplete": (e) => {
        e.target.classList.add('error-field')
        e.target.classList.remove('success-field')
    },
    "oncomplete": (e) => {
        e.target.classList.add('success-field')
        e.target.classList.remove('error-field')
    },
});
im.mask(phoneFields);

// --- обработчик событий ввода в поля имени
nameFields.forEach(el => {
    el.addEventListener('input', e => {
        if (isValidate(e.target)) {
            e.target.classList.add('success-field')
            e.target.classList.remove('error-field')
        } else {
            e.target.classList.add('error-field')
            e.target.classList.remove('success-field')
        }
    })
})

// --- обработчик событий policyCheckBoxes на внешний вид по изменению
policyCheckBoxes.forEach(el => {
    el.addEventListener('change', e => {
        let currentPolicy = e.target.closest('.callback-form').querySelector('.js-policy-ref')
        if (e.target.checked) {
            el.classList.remove('error-field')
            if (currentPolicy.closest('#dynamicCallForm')) currentPolicy.style.color = successColorDynamicForm
            if (currentPolicy.closest('#serviceForm')) currentPolicy.style.color = successColorServiceForm
        } else {
            el.classList.add('error-field')
            currentPolicy.style.color = errorColor
        }
    })
})

// Модуль анимации появления окна сообщения о ПРИНЯТОЙ ЗАЯВКЕ на звонок
function sendMessageSuccess(idForm) {
    const successMessage = document.querySelector(`${idForm} .success-message`)
    let tl = gsap.timeline();
    tl
        .to([`${idForm} ._req`,
            `${idForm} .callback-form__policy-wrapper`,
            `${idForm} .callback-form__submit`], {
            duration: 0.5,
            delay: 1,
            x: 100,
            ease: "back.out(1.7)",
            autoAlpha: 0,
            stagger: {
                each: 0.075,
                from: "edges"
            },
        })
        .fromTo(successMessage, {x: 100}, {
            duration: 1,
            delay: 0.5,
            x: 0,
            autoAlpha: 1,
            zIndex: 5,
            ease: "back.out(1.2)",
        })
    return tl;
}

// проверка принятой политики
const checkPolicy = (form) => {
    const policyField = form.querySelector('.callback-form__policy-checkbox[name="policy"]')
    if (!policyField.checked) {
        form.querySelector('.js-policy-ref').style.color = errorColor
        policyField.classList.add('error-field')
    }
}

// проверка полей на валидацию
const checkValidation = (form) => {
    let fields = form.querySelectorAll('._req')
    fields.forEach(el => {
        // если поля для заполнения пустые
        if (el.value === "") {
            el.classList.remove('success-field')
            el.classList.add('error-field')
        }
        // если эти поля не прошли валидацию
        if (el.classList.contains('error-field') && !el.classList.contains('callback-form__policy-checkbox')) {
            let tweenErrorField = gsap.timeline({repeat: 3})
            tweenErrorField
                .fromTo(el, {x: 0}, {
                    duration: 0.05,
                    x: -75,
                    ease: "sine.out"
                })
                .fromTo(el, {x: -75}, {
                    duration: 0.05,
                    x: 75,
                    ease: "sine.out"
                })
                .fromTo(el, {x: 75}, {
                    duration: 0.05,
                    x: 0,
                    ease: "sine.out"
                })
        }
    })
}

// формирование POST запроса через captcha
const requestViaCaptcha = (form, formData) => {
    grecaptcha.ready(() => {
        grecaptcha.execute('6Le9go4bAAAAAL79RMndfTNfTxnXOH64b_HI9JB9', {action: 'homepage'}).then(function(token) {
            serviceForm.querySelector('.token').value = token
            formData.append('token', token.toString());
            fetch("captcha.php", {
                method: "POST",
                body: formData
            })
                .then((response) => response.json())
                .then((result) => {
                    serviceForm.reset()
                })
        })
    })
}

if (serviceForm) {
    // --- обработчик формы заявки с блока калькулятора
    serviceForm.addEventListener('submit', e => {
        e.preventDefault()
        checkPolicy(serviceForm)
        checkValidation(serviceForm)
        if (serviceForm.querySelectorAll('.error-field').length === 0) {
            sendMessageSuccess('#serviceForm')
            let selectedService = document.querySelector(`.calc__service-btn.${class_AnimateBtnActive}`).dataset.service
            let formData = new FormData(serviceForm)
            let arr = []
            formData.append('service', selectedService);

            // очищаем нулевые поля с formData
            for (let [key, value] of formData.entries()) {
                if (value === '0' || key === 'policy') arr.push(key)
            }
            arr.forEach(el => formData.delete(el))

            requestViaCaptcha(serviceForm, formData)
        }
    })
}

if (dynamicCallForm) {
    // --- обработчик формы ПЕРЕЗВОНИТЬ
    dynamicCallForm.addEventListener('submit', e => {
        e.preventDefault()
        checkPolicy(dynamicCallForm)
        checkValidation(dynamicCallForm)
        // проверка на наличие полей не прошедших валидацию
        if (dynamicCallForm.querySelectorAll('.error-field').length === 0) {
            sendMessageSuccess('#dynamicCallForm')

            let formData = new FormData(dynamicCallForm)

            requestViaCaptcha(dynamicCallForm, formData)
        }
    })
}