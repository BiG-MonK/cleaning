import {stepper} from '../functions/stepper'
import vars from '../_vars'

// Модуль появления НАЧАЛЬНОГО ШАГА
export function showServiceInfoNone() {
    let tl = gsap.timeline();
    tl.to(vars.calcServiceInfoNone, {
        duration: 0.5,
        // delay: 1.75,
        x: 0,
        ease: "back.out(1.7)",
        autoAlpha: 1,
    })
    return tl;
}
// Модуль анимации появления ВСЕХ кнопок услуг
export function returnCalcServiceButtons() {
    let tl = gsap.timeline();
    tl.to(vars.calcServiceButtons, {
        duration: 2.5,
        x: 0,
        ease: "elastic.out(1, 0.5)",
        autoAlpha: 1,
        stagger: {
            each: 0.075,
            from: "center"
        }
    })
    return tl;
}

if (document.querySelector('.calc')) {
    const
        class_ServiceInfoActive = 'service-info--active',
        class_AnimateBtnActive = 'service-btn--active',
        calculateBtnMarginLeft = parseInt(getComputedStyle(vars.calculateBtn).marginLeft),
        calculateBtnWidth = parseInt(getComputedStyle(vars.calculateBtn).width),
        calcServiceInfoMargiLeft = parseInt(getComputedStyle(vars.calcServiceInfo).marginLeft),
        maintenanceServiceData = {
            space: {
                'less30': 2000,
                '31-40': 2500,
                '41-50': 3000,
                '51-60': 3500,
                '61-70': 4000,
                '71-80': 4500,
                '81-90': 5000,
                '91-100': 5500,
                'more100': 6000  // 6000 + 45 руб/м2
            },
            novice: 15,
        },
        generalServiceData = {
            space: {
                'less30': 3000,
                '31-40': 4000,
                '41-50': 5000,
                '51-60': 6000,
                '61-70': 7000,
                '71-80': 8000,
                '81-90': 9000,
                '91-100': 10000,
                'more100': 10000  // 10000 + 65 руб/ м2
            },
            novice: 15,
        },
        renovationServiceData = {
            space: {
                'less30': 5500,
                '31-40': 6500,
                '41-50': 7500,
                '51-60': 8500,
                '61-70': 9500,
                '71-80': 10500,
                '81-90': 11500,
                '91-100': 12500,
                'more100': 12500  // 12500+ 95 руб/м2
            },
            novice: 15,
        },
        windowsServiceData = {
            mosquito_nets: 100,
            glass_partitions_one_side: 160,
            glass_partitions_two_side: 230,
            windows_after_renovation: 350,
            novice: 15,
        },
        steamServiceData = {
            two_seater_sofa: 1500,
            armchair: 800,
            tree_seater_sofa: 1600,
            corner_sofa: 1700,
            multi_seat_sofa: 500,
            sofa_bed_inside: 1500,
            chair_bed_inside: 800,
            double_mattress: 500,
            chair: 200,
            carpet: 200,
            stuffed_toys: 50,
            tulle_curtains: 300,
            radiator_treatment: 300,
            delivery_tools: 500
        }

    let totalPrice = 0
    let isDiscount = 0
    let formData = new FormData(vars.serviceForm)
    let isTouchPad = window.innerWidth < 1024
    let isMobile = window.innerWidth < 576

    // Модуль появления КОНЕЧНОГО ШАГА
    function showServiceInfoFinish() {
        let tl = gsap.timeline();
        tl.to(vars.calcServiceInfoFinish, {
            duration: 0,
            x: 0,
            ease: "back.out(1.7)",
            autoAlpha: 1,
        })
        return tl;
    }
    // Модуль появления блока расчета итоговой цены в конечном шаге
    function showServiceInfoPrice() {
        let tl = gsap.timeline();
        tl.to(vars.calcServiceInfoPriceWrap, {
            duration: 1,
            x: 0,
            ease: "back.out(1)",
            autoAlpha: 1,
        })
        return tl;
    }
    // Модуль появления блока просьбы телефона в конечном шаге
    function showServiceInfoGivePhone() {
        let tl = gsap.timeline();
        tl.to(vars.calcServiceInfoGivePhone, {
            duration: 1,
            x: 0,
            ease: "back.out(1)",
            autoAlpha: 1,
        })
        return tl;
    }
    // Модуль появления блока формы в конечном шаге
    function showServiceInfoFinishForm() {
        let tl = gsap.timeline();
        tl.to(vars.calcServiceInfoFinishForm, {
            duration: 1,
            x: 0,
            ease: "back.out(1)",
            autoAlpha: 1,
        })
        return tl;
    }

    // Модуль анимации появления ДОП.УСЛУГ
    function showServiceInfoAdditional() {
        let tl = gsap.timeline();
        tl.to(vars.additionalServiceInfo, {
            duration: 0.85,
            x: 0,
            ease: "back.out(1.7)",
            autoAlpha: 1,
        })
        return tl;
    }
    // Модуль анимации исчезновения ДОП.УСЛУГ
    function leaveServiceInfoAdditional() {
        let tl = gsap.timeline();
        tl.to(vars.additionalServiceInfo, {
            duration: 0.5,
            x: 100,
            ease: "back.out(1.7)",
            autoAlpha: 0,
            onComplete: () => {
                gsap.to(vars.calculateBtn, {
                    duration: 0.75,
                    width: calculateBtnWidth,
                    x: -150,
                    marginLeft: calculateBtnMarginLeft,
                    autoAlpha: 0,
                    ease: "sine.out",
                })
                gsap.to('.calc__service-list', {
                    duration: 0.75,
                    width: "auto",
                    ease: "sine.out",
                })
                gsap.to(vars.calcServiceInfo, {
                    duration: 0.75,
                    marginLeft: calcServiceInfoMargiLeft,
                    ease: "sine.out",
                })
                vars.additionalServiceInfo.classList.remove(class_ServiceInfoActive)
            }
        })
        return tl;
    }
    // Модуль анимации исчезновения ДОП.УСЛУГ по клику кнопки НАВЕСТИ ЧИСТОТУ
    function leaveServiceInfoAdditionalClickOrder() {
        let tl = gsap.timeline();
        vars.calcServiceInfoFinish.classList.add(class_ServiceInfoActive)
        tl.add(leaveServiceInfoAdditional())
        return tl;
    }
    // Модуль анимации исчезновения ДОП.УСЛУГ по клику кнопки ВЕРНУТЬСЯ К УСЛУГАМ
    function leaveServiceInfoAdditionalClickReturn() {
        let tl = gsap.timeline();
        vars.calcServiceInfoNone.classList.add(class_ServiceInfoActive)
        tl.add(leaveServiceInfoAdditional())
        document.querySelector('.calc__service-btn[data-service="additional_services"]').classList.remove(class_AnimateBtnActive)
        return tl;
    }
    // Модуль анимации возвращения высоты основного блока ServiceInfo
    function returnStaticHeightServiceInfo() {
        let tl = gsap.timeline();
        let staticHeight
        if (isTouchPad) staticHeight = 520
        if (isMobile) staticHeight = 650
        tl.to(vars.calcServiceInfo, {
            duration: 0.85,
            height: staticHeight,
            ease: "sine.out",
        })
        return tl;
    }

    // Модуль анимации клика на КНОПКИ УСЛУГ
    function choiceServiceBtnModal(lastServiceInfo, currentServiceInfo) {
        let tl = gsap.timeline();
        vars.calcServiceButtons.forEach(el => el.setAttribute("disabled", "disabled"))
        vars.calculateBtn.setAttribute("disabled", "disabled")
        leaveBtnNext()
        tl
            .to(lastServiceInfo, {
                duration: 0.85,
                delay: 0.5,
                x: 100,
                ease: "back.out(1.7)",
                autoAlpha: 0,
            })
            .to(currentServiceInfo, {
                duration: 0.85,
                x: 0,
                ease: "back.out(1.7)",
                autoAlpha: 1,
                onComplete: () => {
                    if (getComputedStyle(vars.calculateBtn).opacity > 0) {
                        let tweenCalculateBtn = gsap.timeline({repeat: 3})
                        tweenCalculateBtn
                            .fromTo(vars.calculateBtn, {x: 0}, {
                                duration: 0.075,
                                x: -5,
                                ease: "sine.out"
                            })
                            .fromTo(vars.calculateBtn, {x: -5}, {
                                duration: 0.075,
                                x: 0,
                                ease: "sine.out"
                            })
                    } else {
                        gsap.to(vars.calculateBtn, {
                            duration: 1.2,
                            x: 0,
                            ease: "bounce.out",
                            autoAlpha: 1,
                            zIndex: 1
                        })
                    }
                    vars.calcServiceButtons.forEach(el => el.removeAttribute("disabled"))
                    vars.calculateBtn.removeAttribute("disabled")
                }
            })
        return tl;
    }

    // Модуль анимации исчезновения ВСЕХ кнопок услуг
    function leaveCalcServiceButtons() {
        let tl = gsap.timeline();
        tl.to(vars.calcServiceButtons, {
            duration: 0.5,
            x: -300,
            ease: "sine.out",
            autoAlpha: 0,
            stagger: {
                each: 0.075,
                from: "center"
            },
        })
        return tl;
    }
    // Модуль анимации появления кнопки ДАЛЕЕ
    function showBtnNext() {
        let tl = gsap.timeline();
        if (getComputedStyle(vars.calculateBtnNext).opacity < 1) {
            tl.to(vars.calculateBtnNext, {
                duration: 0.75,
                x: 0,
                y: 0,
                ease: "back.out(1.7)",
                autoAlpha: 1,
            })
        }
        return tl;
    }
    // Модуль анимации исчезновения кнопки ДАЛЕЕ
    function leaveBtnNext() {
        let tl = gsap.timeline();
        if (getComputedStyle(vars.calculateBtnNext).opacity > 0) {
            tl.to(vars.calculateBtnNext, {
                duration: 0.75,
                x: 120,
                y: 120,
                ease: "back.out(1.2)",
                autoAlpha: 0,
            })
        }
        return tl;
    }
    // Модуль появления кнопки НАВЕСТИ ЧИСТОТУ в доп.услугах
    function showAdditionalBtnOrder() {
        let tl = gsap.timeline();
        tl.to(vars.additionalBtnOrder, {
            duration: 0.85,
            x: 0,
            y: 0,
            ease: "back.out(1.7)",
            autoAlpha: 1,
        })
        return tl;
    }
    // Модуль исчезновения кнопки НАВЕСТИ ЧИСТОТУ в доп.услугах
    function leaveAdditionalBtnOrder() {
        let tl = gsap.timeline();
        tl.to(vars.additionalBtnOrder, {
            duration: 0.5,
            x: 120,
            y: 120,
            ease: "back.out(1.2)",
            autoAlpha: 0,
        })
        return tl;
    }
    // Модуль появления кнопки ВЕРНУТЬСЯ К УСЛУГАМ в доп.услугах
    function showAdditionalBtnReturn() {
        let tl = gsap.timeline();
        tl.to(vars.additionalBtnReturn, {
            duration: 0.85,
            x: 0,
            y: 0,
            ease: "back.out(1.7)",
            autoAlpha: 1,
        })
        return tl;
    }
    // Модуль исчезновения кнопки ВЕРНУТЬСЯ К УСЛУГАМ в доп.услугах
    function leaveAdditionalBtnReturn() {
        let tl = gsap.timeline();
        tl.to(vars.additionalBtnReturn, {
            duration: 0.5,
            x: -120,
            y: -120,
            ease: "back.out(1.2)",
            autoAlpha: 0,
        })
        return tl;
    }

    // --- обработчики полей input type="text" на изменение
    document.querySelectorAll('.service-info input[type="text"]:not(.callback-form__field)').forEach(el => {
        el.addEventListener('input', e => {
            showBtnNext()
        })
    })

    // --- обработчики полей input type="radio" на изменение
    document.querySelectorAll('.service-info input[type="radio"]').forEach(el => {
        el.addEventListener('input', e => {
            showBtnNext()
        })
    })

    // --- обработчики кнопок услуг калькулятора
    vars.calcServiceButtons.forEach(el => {
        el.addEventListener('click', e => {
            if (window.innerWidth < 1024) {
                window.scrollTo({
                    top: window.pageYOffset + vars.calculateBtn.getBoundingClientRect().top - 15,
                    behavior: "smooth",
                });
            }
            let currentService = e.currentTarget.dataset.service,
                lastServiceBtn = document.querySelector(`.calc__service-btn.${class_AnimateBtnActive}`),
                lastServiceInfo = document.querySelector(`.${class_ServiceInfoActive}`),
                currentServiceInfo

            // очистка всех полей ввода в блоке calc и расчетных переменных
            vars.serviceForm.reset()
            totalPrice = 0
            isDiscount = 0

            if (e.currentTarget !== lastServiceBtn) {
                // выявление актуального service-info__item
                document.querySelector(`.${class_ServiceInfoActive}`).classList.remove(class_ServiceInfoActive)
                vars.calcServiceInfoItems.forEach(el => {
                    if (el.dataset.service === currentService) {
                        el.classList.add(class_ServiceInfoActive)
                        currentServiceInfo = el
                    }
                })
                // переназначение класса активности на кнопках услуг
                if (lastServiceBtn !== null) {
                    if (lastServiceBtn !== e.currentTarget) {
                        lastServiceBtn.classList.remove(class_AnimateBtnActive)
                        e.currentTarget.classList.add(class_AnimateBtnActive)
                    }
                } else e.currentTarget.classList.add(class_AnimateBtnActive)

                if (currentService === 'additional_services') {
                    isTouchPad ? onClickAdditionalBtnMobile(lastServiceInfo) : onClickAdditionalBtn(lastServiceInfo)
                } else {
                    isTouchPad ? choiceServiceBtnMobile(lastServiceInfo, currentServiceInfo) : choiceServiceBtn(lastServiceInfo, currentServiceInfo)
                }
            }
        })
    })

    // --- обработчики кнопоки РАССЧИТАТЬ
    vars.calculateBtn.addEventListener('click', e => {
        let lastServiceInfo = document.querySelector(`.${class_ServiceInfoActive}`),
            selectedService = document.querySelector(`.calc__service-btn.${class_AnimateBtnActive}`).dataset.service,
            nextStepEl = document.querySelector(`.service-info__item[data-service="${selectedService}--step"][data-step="2"]`)

        lastServiceInfo.classList.remove(class_ServiceInfoActive)
        nextStepEl.classList.add(class_ServiceInfoActive)

        onClickCalculateBtn(lastServiceInfo, nextStepEl)
    })

    // --- обработчики кнопоки ДАЛЕЕ
    vars.calculateBtnNext.addEventListener('click', e => {
        let lastServiceInfo = document.querySelector(`.${class_ServiceInfoActive}`),
            selectedService = document.querySelector(`.calc__service-btn.${class_AnimateBtnActive}`).dataset.service,
            lastStepEl = lastServiceInfo.dataset.step,
            nextStepEl = Number(lastStepEl) + 1,
            nextServiceInfo = document.querySelector(`.service-info__item[data-service="${selectedService}--step"][data-step="${nextStepEl}"]`)

        lastServiceInfo.classList.remove(class_ServiceInfoActive)
        if (nextServiceInfo === null) {             // Если следующий шаг - последний
            // Восстановление параметров блоков сообщений, формы обратной связи и полей
                gsap.set([
                    '#serviceForm ._req',
                    '#serviceForm .callback-form__policy-wrapper',
                    '#serviceForm .callback-form__submit'], {
                    x: 0,
                    autoAlpha: 1,
                })
                gsap.set('#serviceForm .success-message', {
                    autoAlpha: 0,
                    zIndex: -1
                })
            vars.serviceForm.querySelectorAll('._req').forEach(el => el.classList.remove('success-field'))
            vars.serviceForm.querySelector('.js-policy-ref').style.color = 'inherit'
            document.querySelector('.js-total-price-field').value = '0'

            // Расчет суммы, анализ заполненных полей
            let serviceData
            let filledFields = []
            formData = new FormData(vars.serviceForm)
            for (let [key, value] of formData.entries()) {
                if (value !== '0' && value != null && value !== '') {
                    filledFields.push([key, value])
                }
            }
            switch (selectedService) {
                case 'maintenance_cleaning':
                    serviceData = maintenanceServiceData
                    break
                case 'general_cleaning':
                    serviceData = generalServiceData
                    break
                case 'cleaning_up_after_renovation':
                    serviceData = renovationServiceData
                    break
                case 'steam_cleaner':
                    serviceData = steamServiceData
                    break
                case 'washing_windows':
                    serviceData = windowsServiceData
                    break
            }
            filledFields.forEach(el => {
                if (el[0] === 'novice') {
                    isDiscount = serviceData[el[0]]
                } else {
                    if (el[0] !== 'space') {
                        totalPrice += serviceData[el[0]] * el[1]
                    } else {
                        totalPrice = serviceData[el[0]][el[1]]
                    }
                }
            })
            // Если выбрана услуга ОБРАБОТКА ПАРОМ то +500 за доставку прибора
            // if (serviceData === steamServiceData) totalPrice += steamServiceData['delivery_tools']

            totalPrice = totalPrice - (totalPrice * (isDiscount / 100))
            document.querySelector('.js-total-price-field').value = totalPrice
            vars.serviceTotalPriceEl.innerHTML = '0'

            // переключение класса на активное окно и запуск анимации
            vars.calcServiceInfoFinish.classList.add(class_ServiceInfoActive)
            onClickCalculateBtnNextLastStep(lastServiceInfo, vars.calcServiceInfoFinish)
        } else {
            nextServiceInfo.classList.add(class_ServiceInfoActive)
            onClickCalculateBtnNext(lastServiceInfo, nextServiceInfo)
        }
    })

    // --- обработчики кнопки ВОЗВРАТА с доп. услуг
    vars.additionalBtnReturn.addEventListener('click', e => {
        isTouchPad ? onClickAdditionalBtnReturnMobile() : onClickAdditionalBtnReturn()
    })

    // --- обработчики кнопоки в разделе доп услуг НАВЕСТИ ЧИСТОТУ
    vars.additionalBtnOrder.addEventListener('click', e => {
        isTouchPad ? onClickAdditionalBtnOrderMobile() : onClickAdditionalBtnOrder()
    })

    // --- обработчики степперов в окнах
    vars.steppers.forEach(el => {
        stepper(el)
    })

    // Анимация при клике на КНОПКИ УСЛУГ в блоке калькуляторе
    function choiceServiceBtn(lastServiceInfo, currentServiceInfo) {
        const tween = gsap.timeline({repeat: 0})
        tween.add(choiceServiceBtnModal(lastServiceInfo, currentServiceInfo))
    }

    // Анимация при клике на кнопку РАССЧИТАТЬ
    function onClickCalculateBtn(lastServiceInfo, nextStepEl) {
        const tween = gsap.timeline({repeat: 0})
        tween
            .to(vars.calculateBtn, {
                duration: 0.85,
                x: -50,
                autoAlpha: 0,
                zIndex: -1,
                ease: "back.out(1.7)",
                onStart: () => vars.calcServiceButtons.forEach(el => el.setAttribute("disabled", "disabled"))
            })
            .to(lastServiceInfo, {
                duration: 0.85,
                x: 100,
                ease: "back.out(1.7)",
                autoAlpha: 0,
            })
            .to(nextStepEl, {
                duration: 0.85,
                x: 0,
                ease: "back.out(1.7)",
                autoAlpha: 1,
                onComplete: () => vars.calcServiceButtons.forEach(el => el.removeAttribute("disabled"))
            })
    }

    // Анимация при клике на кнопку ДАЛЕЕ
    function onClickCalculateBtnNext(lastServiceInfo, nextServiceInfo) {
        const tween = gsap.timeline({repeat: 0})
        vars.calcServiceButtons.forEach(el => el.setAttribute("disabled", "disabled"))
        tween
            .to(lastServiceInfo, {
                duration: 0.85,
                x: 100,
                ease: "back.out(1.7)",
                autoAlpha: 0,
            })
            .add(leaveBtnNext())
            .to(nextServiceInfo, {
                duration: 0.85,
                x: 0,
                ease: "back.out(1.7)",
                autoAlpha: 1,
            })
            .add(() => vars.calcServiceButtons.forEach(el => el.removeAttribute("disabled")))
    }

    // Анимация при клике на кнопку ДАЛЕЕ последний шаг
    function onClickCalculateBtnNextLastStep(lastServiceInfo, calcServiceInfoFinish) {
        gsap.set(['.service-info__give-phone', '.service-info__description-wrap', vars.calcServiceInfoFinishForm], {
            x: "120%",
            autoAlpha: 0
        })
        gsap.set('.service-info__price-wrap', {
            x: 0,
            autoAlpha: 0,
            display: 'block'
        })
        gsap.set('.service-info__give-phone', {
            marginBottom: 15
        })
        const tween = gsap.timeline({repeat: 0})
        vars.calcServiceButtons.forEach(el => el.setAttribute("disabled", "disabled"))
        tween
            .to(lastServiceInfo, {
                duration: 0.85,
                x: 100,
                ease: "back.out(1.7)",
                autoAlpha: 0,
            })
            .add(leaveBtnNext())
            .add(showServiceInfoFinish())
            .add(showServiceInfoPrice())
            .to('.service-info__description-wrap', {
                duration: 1,
                x: 0,
                ease: "back.out(1)",
                autoAlpha: 1,
                onComplete: () => {
                    // Анимированный счетчик итоговой суммы
                    let num = {var: 0}
                    gsap.timeline()
                        .set(num, {var: 0}) // back to '0'
                        .to(num, {
                            var: totalPrice,
                            duration: 2.5,
                            ease: "power3.out",
                            onStart: () => document.querySelector('.service-info__price-rub').textContent = '₽',
                            onUpdate: () => {
                                vars.serviceTotalPriceEl.innerHTML = (num.var).toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
                            }
                        })
                }
            })
            .add(showServiceInfoGivePhone(), "+=2")
            .add(showServiceInfoFinishForm())
            .add(() => {vars.calcServiceButtons.forEach(el => el.removeAttribute("disabled"))})
    }

    // Анимация при клике на кнопку ДОПОЛНИТЕЛЬНЫХ услуг, т.к. большая таблица
    function onClickAdditionalBtn(lastServiceInfo) {
        const tween = gsap.timeline({repeat: 0})
        tween
            .add(leaveBtnNext())
            .add(leaveCalcServiceButtons())
            .to(vars.calculateBtn, {
                duration: 0.5,
                x: -150,
                autoAlpha: 0,
                ease: "sine.out",
            })
            .to(lastServiceInfo, {
                duration: 0.5,
                x: 100,
                ease: "sine.out",
                autoAlpha: 0,
                onComplete: () => {
                    gsap.to(vars.calculateBtn, {
                        duration: 1.1,
                        width: 0,
                        margin: 0,
                        ease: "bounce.out",
                    })
                    gsap.to('.calc__service-list', {
                        duration: 1.1,
                        width: 0,
                        ease: "bounce.out",
                    })
                    gsap.to(vars.calcServiceInfo, {
                        duration: 1.1,
                        marginLeft: 0,
                        ease: "bounce.out",
                    })
                }
            })
            .add(showServiceInfoAdditional(), "+=1")
            .add(showAdditionalBtnReturn())
            .add(showAdditionalBtnOrder())
    }

    //ДЛЯ МОБИЛ < 1024 Анимация при клике на КНОПКИ УСЛУГ в блоке калькуляторе
    function choiceServiceBtnMobile(lastServiceInfo, currentServiceInfo) {
        const tween = gsap.timeline({repeat: 0})
        tween
            .add(leaveAdditionalBtnOrder())
            .add(leaveAdditionalBtnReturn())
            .add(choiceServiceBtnModal(lastServiceInfo, currentServiceInfo))
            .add(returnStaticHeightServiceInfo(), "-=1.5")
    }

    //ДЛЯ МОБИЛ < 1024 Анимация при клике на кнопку ДОПОЛНИТЕЛЬНЫХ услуг, т.к. большая таблица
    function onClickAdditionalBtnMobile(lastServiceInfo) {
        const tween = gsap.timeline({repeat: 0})
        tween
            .add(leaveBtnNext())
            .to(vars.calculateBtn, {
                duration: 0.5,
                x: -150,
                autoAlpha: 0,
                ease: "sine.out",
            })
            .to(lastServiceInfo, {
                duration: 0.5,
                x: 100,
                ease: "sine.out",
                autoAlpha: 0,
            })
            .to(vars.calcServiceInfo, {
                duration: 0.85,
                height: "auto",
                ease: "bounce.out",
            })
            .add(showServiceInfoAdditional())
            .add(showAdditionalBtnReturn())
            .add(showAdditionalBtnOrder())
    }

    //ДЛЯ МОБИЛ < 1024 Анимация при нажатии на кнопку ВЕРНУТЬСЯ К УСЛУГАМ в блоке ДОП УСЛУГИ
    function onClickAdditionalBtnReturnMobile() {
        const tween = gsap.timeline({repeat: 0})
        tween
            .add(leaveAdditionalBtnOrder())
            .add(leaveAdditionalBtnReturn())
            .add(leaveServiceInfoAdditionalClickReturn())
            .add(returnStaticHeightServiceInfo())
            .add(showServiceInfoNone())
    }

    //ДЛЯ МОБИЛ < 1024 Анимация при нажатии на кнопку НАВЕСТИ ЧИСТОТУ в блоке ДОП УСЛУГИ
    function onClickAdditionalBtnOrderMobile() {
        const tween = gsap.timeline({repeat: 0})
        gsap.set(['.service-info__price-wrap', '.service-info__give-phone', '.service-info__description-wrap', vars.calcServiceInfoFinishForm], {
            x: "120%",
            autoAlpha: 0,
        })
        gsap.set('.service-info__price-wrap', {
            display: 'none'
        })
        gsap.set('.service-info__give-phone', {
            marginBottom: 30
        })
        tween
            .add(leaveAdditionalBtnOrder())
            .add(leaveAdditionalBtnReturn())
            .add(leaveServiceInfoAdditionalClickOrder())
            .add(returnStaticHeightServiceInfo())
            .add(showServiceInfoFinish())
            .add(showServiceInfoGivePhone())
            .add(showServiceInfoFinishForm())
            .add(returnCalcServiceButtons())
    }

    // Анимация при нажатии на кнопку ВЕРНУТЬСЯ К УСЛУГАМ в блоке ДОП УСЛУГИ
    function onClickAdditionalBtnReturn() {
        const tween = gsap.timeline({repeat: 0})
        tween
            .add(leaveAdditionalBtnOrder())
            .add(leaveAdditionalBtnReturn())
            .add(leaveServiceInfoAdditionalClickReturn())
            .add(showServiceInfoNone(), "+=1.5")
            .add(returnCalcServiceButtons())
    }

    // Анимация при нажатии на кнопку НАВЕСТИ ЧИСТОТУ в блоке ДОП УСЛУГИ
    function onClickAdditionalBtnOrder() {
        const tween = gsap.timeline({repeat: 0})
        gsap.set(['.service-info__price-wrap', '.service-info__give-phone', '.service-info__description-wrap', vars.calcServiceInfoFinishForm], {
            x: "120%",
            autoAlpha: 0,
        })
        gsap.set('.service-info__price-wrap', {
            display: 'none'
        })
        gsap.set('.service-info__give-phone', {
            marginBottom: 30
        })
        tween
            .add(leaveAdditionalBtnOrder())
            .add(leaveAdditionalBtnReturn())
            .add(leaveServiceInfoAdditionalClickOrder())
            .add(showServiceInfoFinish(), "+=1.5")
            .add(showServiceInfoGivePhone())
            .add(showServiceInfoFinishForm())
            .add(returnCalcServiceButtons())
    }
}