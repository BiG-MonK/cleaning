
const callMeBtn = document.querySelector('.callback-btn')
const callMePhone = document.querySelector('.callback-btn__text-call')
const callMePhoneText = document.querySelector('.callback-btn__text')
const callMeFormWrapper = document.querySelector('.callback-btn__form-wrapper')
const callMeFormClose = document.querySelector('.callback-btn__close')

callMeBtn.addEventListener('click', e => {

    // обработчик на закрытие окна ПЕРЕЗВОНИТЬ
    if (e.target === callMeFormClose) {
        const callMeClose = gsap.timeline({repeat: 0})
        callMeClose
            .to(callMeFormWrapper, {
                duration: 0.5,
                autoAlpha: 0,
                ease: "sine.out",
                onComplete: () => {
                    callMeFormWrapper.style.display = "none"
                },
            })
            .to(callMeBtn, {
                duration: 0.75,
                height: 80,
                width: 80,
                ease: "back.out(1.7)",
            })
            .to(callMePhone, {
                duration: 0.5,
                autoAlpha: 1,
                ease: "sine.out",
                onStart: () => {
                    callMePhone.style.display = "flex"
                    callMePhone.style.cursor = "pointer"
                    callMeBtn.style.cursor = "pointer"
                }
            })
    }

    // обработчик на открытие окна ПЕРЕЗВОНИТЬ
    if (e.target === callMePhoneText || e.target === callMePhone) {
        const callMeOpen = gsap.timeline({repeat: 0})
        let callMeBtnOpenHeight = window.innerWidth < 575 ? 300 : 400,
            callMeBtnOpenWidth = window.innerWidth < 575 ? 300 : 400

        callMeOpen
            .to(callMePhone, {
                duration: 0.5,
                autoAlpha: 0,
                ease: "sine.out",
                onComplete: () => {
                    callMePhone.style.display = "none"
                    callMePhone.style.cursor = "default"
                    callMeBtn.style.cursor = "default"
                }
            })
            .to(callMeBtn, {
                duration: 0.75,
                height: callMeBtnOpenHeight,
                width: callMeBtnOpenWidth,
                ease: "back.out(1.7)",
            })
            .to(callMeFormWrapper, {
                onStart: () => {
                    callMeFormWrapper.style.display = "flex"
                },
                duration: 0.5,
                autoAlpha: 1,
                ease: "sine.out",
            })
    }
})