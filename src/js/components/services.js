import vars from '../_vars'
import {returnCalcServiceButtons} from "./calc";

// imagesLoaded('.slideshow', { background: true }, function() {
    // скрыть лоадер
    // document.querySelector('.loader').classList.add('is-loaded')
if (document.querySelector('.services')) {
    let rotation,
        type = '_short'

    // Fix наслоения блока navigation на кнопку ПОДРОБНЕЕ
    vars.navigation.addEventListener('click', e => {
        const
            targetBtn = document.querySelector('.detail-item.active .detail-item__btn'),
            btn = targetBtn.getBoundingClientRect()
        let x0 = btn.left,
            x1 = x0 + btn.width,
            y0 = btn.top,
            y1 = y0 + btn.height,
            clickX = e.clientX,
            clickY = e.clientY

        if (clickX > x0 && clickX < x1 && clickY > y0 && clickY < y1) {
            targetBtn.closest('.detail-item__btn-wrap').click()
        }
    })

    // --- обработчики кнопок ПОДРОБНЕЕ
    vars.detailButtonsWrap.forEach(el => {
        el.addEventListener('click', e => {
            let dataSet = document.querySelector('.detail-item.active').dataset.service
            let targetBtn = document.querySelector(`.calc__service-btn[data-service="${dataSet}"]`)

            if (window.innerWidth < 1024) {
                if (ScrollTrigger.getById("calcStart")) ScrollTrigger.getById("calcStart").kill(true)
                    returnCalcServiceButtons()
                    targetBtn.click()
            } else {
                const calcBlock = document.querySelector('.calc')
                window.scrollTo({
                    top: window.pageYOffset + calcBlock.getBoundingClientRect().top,
                    behavior: "smooth",
                })
                if (dataSet !== 'additional_services') {   // Если выбраны НЕ доп услуги
                    if (ScrollTrigger.getById("calcStart")) ScrollTrigger.getById("calcStart").kill(true)
                    if (vars.additionalServiceInfo.closest('.service-info--active')) {
                        vars.additionalBtnReturn.click()
                        setTimeout(() => {
                            targetBtn.click()
                        }, 2500)
                    } else {
                        setTimeout(() => {
                            returnCalcServiceButtons()
                            targetBtn.click()
                        }, 1000)
                    }
                } else {
                    // Если любая кнопка услуг уже выбиралась ранее
                    if (document.querySelector('.service-btn--active')) {
                        setTimeout(() => targetBtn.click(), 500)
                    } else setTimeout(() => targetBtn.click(), 3500)
                }
            }
        })
    })

    // --- обработчик наведения мышки на кнопку ПОДРОБНЕЕ
    vars.detailButtons.forEach(el => {
        el.addEventListener('mouseenter', e => {
            e.target.classList.add('animateWave')
        })
    })

    // --- обработчик отведения мышки с кнопки ПОДРОБНЕЕ
    vars.detailButtons.forEach(el => {
        el.addEventListener('mouseleave', e => {
            setTimeout(() => {
                e.target.classList.remove('animateWave')
            }, 450)
        })
    })

    // --- обработчики navigation-item
    vars.navigationItems.forEach(el => {
        el.addEventListener('click', e => {
            let dataSet = e.target.closest('.navigation-item').dataset.service

            // if (window.innerWidth > 1920) {
            let currentBg = document.querySelector(`[data-service="${dataSet}"] .background`)
            let newBgImage = getComputedStyle(currentBg).backgroundImage
            // фикс для фаерфокса плавности смены фона на широкоформатках
            vars.serviceBgElements.forEach(el => {
                if (!el.classList.contains('services__bg--active')) {
                    el.style.backgroundImage = newBgImage
                }
            })
            // }

            rotation = Number(el.querySelector('.rotate-holder').innerHTML)
            doTween(el)
        })
    })
    vars.navigationItems.forEach(el => {
        el.addEventListener('mouseleave', e => {
            setTimeout(() => {
                e.target.closest('.navigation-item').classList.remove('animateWave')
            }, 450)
        })
    })
    vars.navigationItems.forEach(el => {
        if (window.innerWidth > 1200) {
            el.addEventListener('mouseenter', e => {
                e.target.closest('.navigation-item').classList.add('animateWave')
            })
        }
    })

    // разбиение заголовка на символы
    document.querySelectorAll('.headline').forEach(head => {
        let letters = head.innerHTML.split('')
        head.innerHTML = letters.map(el => `<span class='letter'>${el}</span>`).join('')
    })

    function doTween(target) {
        const tween = gsap.timeline({repeat: 0})
        let lastActiveService = document.querySelectorAll('.active .letter')
        gsap.to(vars.detailButtonsWrap, {
            duration: 0.5,
            x: -100,
            ease: "sine.out",
            autoAlpha: 0
        })
        tween.to(lastActiveService, {
            duration: 0.2,
            autoAlpha: 0,
            x: -100,
            ease: "sine.out",
            stagger: 0.035,
            onComplete: () => {
                vars.serviceBgElements.forEach(el => {
                    el.classList.toggle('services__bg--active')
                })
                vars.navigationItems.forEach(el => {
                    el.classList.remove('active')
                    if (el === target) el.classList.add('active')
                })
                vars.detailItems.forEach(el => {
                    el.classList.remove('active')
                    if (el.dataset.service === target.dataset.service) el.classList.add('active')
                })

                // анимация смены фона services
                gsap.fromTo('.active .background',
                    {
                        autoAlpha: 0.3,
                        scale: 1
                    },
                    {
                        duration: 0.9,
                        autoAlpha: 1,
                        scale: 1.1,
                        ease: "sine.out",
                        stagger: 0.05
                    })

                // анимация поворота фона кружка услуги вместе с вращением
                gsap.to('.background-holder', {
                    duration: 1,
                    rotation: (index, element) => -90 - Number(element.previousElementSibling.innerHTML) + rotation + type,
                    transformOrigin: "50% 50%",
                    ease: "sine.out",
                })

                tween
                    // анимация поворота самих кругов услуг
                    .to(vars.navigation, {
                        duration: 1,
                        rotation: -rotation + type,
                        transformOrigin: "50% 50%",
                        ease: "sine.out",
                    })

                    // анимация появления символов названия услуги
                    .fromTo('.active .letter',
                        {
                            autoAlpha: 0,
                            x: -100,
                        },
                        {
                            duration: 0.3,
                            autoAlpha: 1,
                            x: 0,
                            ease: "sine.out",
                            stagger: 0.05
                        })
                    // анимация появления кнопки ПОДРОБНЕЕ
                    .to(vars.detailButtonsWrap, {
                        duration: 0.5,
                        x: 0,
                        ease: "sine.out",
                        autoAlpha: 1
                    })
            }
        })
    }

    // при загрузке показывать слайд-шоу, а также первый "активный" элемент navigation/detail item
    const tweenStart = gsap.timeline({repeat: 0})
    gsap.set('.active .letter', {
        autoAlpha: 0,
        x: -100
    })
    gsap.set(vars.detailButtonsWrap, {
        autoAlpha: 0,
        x: -100
    })
    tweenStart.to(vars.slideshow, {
        duration: 1,
        autoAlpha: 1,
        onComplete: () => {
            // подготовка навигации и установка элементов в правильное место
            vars.navigationItems.forEach((elem, index) => {
                // выводит начальное положение кружков услуг по кругу с задержкой
                gsap.to(elem, {
                    delay: 0.5,
                    duration: 1.5,
                    opacity: 1,
                    left: vars.navigation.offsetWidth / 2 - elem.offsetWidth / 2 - 10,
                    rotation: 90 + (index * 360 / vars.navigationItems.length),
                    transformOrigin: "50% " + vars.navigation.offsetWidth / 2 + "px"
                })
                // добавляет текст в span элемент для расчета углов
                elem.querySelector('.rotate-holder').innerHTML = String(index * 360 / vars.navigationItems.length)
                // переворачиваеткружки услуг в нормальный угол
                gsap.to(elem.querySelector('.background-holder'), {
                    rotation: -90 - (index * 360 / vars.navigationItems.length),
                })
            })
        }
    })
        .to('.active .letter', {
            delay: 2,
            duration: 0.3,
            autoAlpha: 1,
            x: 0,
            ease: "sine.out",
            stagger: 0.05
        })
        .to('.active .background', {
            duration: 0.7,
            autoAlpha: 1,
            x: 0
        })
        .to(vars.detailButtonsWrap, {
            duration: 0.5,
            x: 0,
            ease: "sine.out",
            autoAlpha: 1
        })
}
// })
