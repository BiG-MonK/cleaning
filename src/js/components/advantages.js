if (document.querySelector('.advantages')) {
    // разбиение заголовка на символы
    let wordFirst = document.querySelector('.advantages__title-first'),
        wordSecond = document.querySelector('.advantages__title-second'),
        lettersWordFirst = wordFirst.innerHTML.split(''),
        lettersWordSecond = wordSecond.innerHTML.split('')
    wordFirst.innerHTML = lettersWordFirst.map(el => `<span class='advantages__letter-ws'>${el}</span>`).join('')
    wordSecond.innerHTML = lettersWordSecond.map(el => `<span class='advantages__letter-ws'>${el}</span>`).join('')

}

