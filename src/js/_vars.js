export default {
//-- блок services
    slideshow: document.querySelector(".slideshow"),
    navigation: document.querySelector(".navigation"),
    navigationItems: document.querySelectorAll(".navigation-item"),
    detailItems: document.querySelectorAll(".detail-item"),
    detailButtonsWrap: document.querySelectorAll(".detail-item__btn-wrap"),
    detailButtons: document.querySelectorAll(".detail-item__btn"),
    serviceBgElements: document.querySelectorAll('.services__bg'),

//-- блок calc
    calcServiceInfo: document.querySelector('.calc__service-info'),
    calcServiceButtons: document.querySelectorAll('.calc__service-btn'),
    calcServiceInfoItems: document.querySelectorAll('.service-info__item'),
    calcServiceInfoFinish: document.querySelector('.service-info__finish'),
    calcServiceInfoNone: document.querySelector('.service-info__none'),
    calcServiceInfoFinishForm: document.querySelector('.service-info__finish .callback-form'),
    calcServiceInfoPriceWrap: document.querySelector('.service-info__price-wrap'),
    calcServiceInfoGivePhone: document.querySelector('.service-info__give-phone'),
    calculateBtn: document.querySelector('.calc__calculate-btn'),
    calculateBtnNext: document.querySelector('.calc-btn__next'),
    additionalServiceInfo: document.querySelector('.service-info__additional'),
    additionalBtnReturn: document.querySelector('.additional__btn--return'),
    additionalBtnOrder: document.querySelector('.additional__btn--order'),
    steppers: document.querySelectorAll('.stepper .stepper__input'),
    serviceForm: document.querySelector('#serviceForm'),
    serviceTotalPriceEl: document.querySelector('.service-info__price'),
}