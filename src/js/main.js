import './components/header'
import './components/services'
import './components/promotions'
import './components/advantages'
import './components/calc'
import './components/faq'
import './components/call_me'
import './components/callback_form'
import './components/policy'
import vars from "./_vars";
import {returnCalcServiceButtons, showServiceInfoNone} from "./components/calc";

gsap.registerPlugin(ScrollTrigger);

const call = document.querySelector('.callback-btn')

let timer = null;
let viewHeight = window.innerHeight
let viewWidth = window.innerWidth
let footerPositionTop = document.querySelector('.footer').getBoundingClientRect().top
let countPixelScroll = 0

// --- Обработчик скрола при достижении блока Footer и периодический возврат CallMe к верху
window.addEventListener('scroll', function() {
    clearTimeout(timer);
    if (footerPositionTop - viewHeight < window.pageYOffset) call.style.opacity = '1'
    timer = setTimeout(function() {
        // if (Math.abs(window.pageYOffset - countPixelScroll) > 150) {
            gsap.to(call, {
                duration: 2.5,
                zIndex: 20,
                ease: "back.out(1.7)",
                top: window.pageYOffset + 50,
            })
            countPixelScroll = window.pageYOffset
        // }
    }, 250);
})

// --- Обработчик на изменения размера ширины окна
window.addEventListener("resize", () => {
    if (viewWidth !== window.innerWidth) window.location.reload()
})

// --- Слушатель "calcStart" на появление блока calc в области видимости
gsap.to('.calc', {
    scrollTrigger: {
        id: "calcStart",
        trigger: ".calc",
        start: "top center",
        // toggleActions: "restart none reverse none",
    },
    onStart: () => {
        // Анимация при первом показе на экране блока калькулятора
        const tween = gsap.timeline({repeat: 0})
        vars.calcServiceButtons.forEach(el => el.setAttribute("disabled", "disabled"))
        tween
            .add(returnCalcServiceButtons())
            .add(showServiceInfoNone(), "-=0.5")
            .add(() => vars.calcServiceButtons.forEach(el => el.removeAttribute("disabled")))
    }
})

// --- Слушатель "advantagesStart" на появление блока advantages в области видимости
gsap.to(['.advantages__count', '.advantages__letter-ws'], {
    scrollTrigger: {
        id: "advantagesStart",
        trigger: ".advantages",
        start: "top 80%",
    },
        duration: 0.3,
        autoAlpha: 1,
        x: 0,
        ease: "sine.out",
        stagger: 0.05,
})
function animateAdvantage() {
    gsap.to('.advantages__item', {
        scrollTrigger: {
            trigger: '.advantages__item',
            start: "25% 80%",
        },
        duration: 1.5,
        delay: 0.5,
        autoAlpha: 1,
        x: 0,
        ease: "back.out(3)",
        stagger: {
            each: 0.15,
        },
    })
}
function animateAdvantageMobile(item) {
    gsap.to(item, {
        scrollTrigger: {
            trigger: item,
            start: "top 80%",
        },
        duration: 1.25,
        delay: 0.5,
        autoAlpha: 1,
        x: 0,
        ease: "back.out(2)",
    })
}
if (window.innerWidth < 576) {
    animateAdvantageMobile('.advantages__item-first')
    animateAdvantageMobile('.advantages__item-second')
    animateAdvantageMobile('.advantages__item-third')
    animateAdvantageMobile('.advantages__item-fourth')
} else animateAdvantage()

// --- Слушатель "faqStart" на появление блока faq в области видимости
gsap.to('.faq__item', {
    scrollTrigger: {
        id: "faqStart",
        trigger: ".faq",
        start: "35% 80%",
    },
    duration: 1.25,
    delay: 0.3,
    autoAlpha: 1,
    y: 0,
    ease: "back.out(4)",
    stagger: {
        each: 0.15,
    },
})

// --- scrollTrigger на ПУЗЫРИ
gsap.to('.bubble--small', {
    scrollTrigger: {
        scrub: 3
    },
    yPercent: 350,
})
gsap.to('.bubble--middle', {
    scrollTrigger: {
        scrub: 2
    },
    delay: 0.5,
    yPercent: 350,
})
gsap.to('.bubble--large', {
    scrollTrigger: {
        scrub: 5
    },
    yPercent: 800,
})