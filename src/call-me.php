<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require 'PHPMailer-6.5.0/src/PHPMailer.php';
    require 'PHPMailer-6.5.0/src/Exception.php';

    $mail = new PHPMailer(true);
    $mail -> CharSet = 'UTF-8';
    $mail -> setLanguage('ru', 'PHPMailer-6.5.0/language');
    $mail -> IsHTML(true);

    // --- От кого письмо
    $mail -> setFrom('info@дарьяклининг.рф', 'Дарья Клининг');

    // --- Кому
    $mail -> addAddress('daryacleaning@gmail.com');

    // --- Тема письма
    $mail -> Subject = 'Требуется перезвонить!';

    // --- Тело письма
    $body = '<h1>Поступил заказ на звонок!</h1>';
    if(trim(!empty($_POST['name']))) {
        $body .= '<p><strong>Имя:</strong> '.$_POST['name'].'</p>';
    }
    if(trim(!empty($_POST['phone']))) {
        $body .= '<p><strong>Телефон:</strong> '.$_POST['phone'].'</p>';
    }

    $mail -> msgHTML($body);

    if (!$mail -> send()) {
        $message = 'error';
    } else {
        $message = 'success';
    }

    $response = ['message' => $message];
    header('Content-type: application/json');
    echo json_encode($response);

